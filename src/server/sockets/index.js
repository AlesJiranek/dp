import config from '../config';
import elasticsearch from 'elasticsearch';
import moment from 'moment';
import importer from '../lib/importer';


const es = new elasticsearch.Client({
  host: config.elasticsearchServer
});


export default function sockets(socket) {

  socket

  .on('search', (index, {term, timeFrom, timeTo, sizeLimit}) => {

    es.search({
      index: index,
      q: term,
      body: {
        '_source': {
          'include': ['page', 'userId', 'userName', 'id', 'parentId', 'level']
        },
        filter: {
          'bool': {
            'must': [{
              'range': {
                'created': {
                  'gte': timeFrom ? moment(timeFrom, 'DD-MM-YYYY').format('x') : moment('1970-01-01').format('x'),
                  'lte': timeTo ? moment(timeTo, 'DD-MM-YYYY').format('x') : moment().format('x')
                }
              }
            }]
          }
        },
        size: sizeLimit ? parseInt(sizeLimit, 10) : config.maxResultSize
      }
    }).then(response => {
      socket.emit('searchResults', response);
    });
  })

  .on('getIndexes', () => {
    es.indices.stats()
      .then(({indices}) => {
        socket.emit('availableIndexes', indices);
      });
  })


  .on('startImport', pageId => {
    const onFinish = () => {
      socket.emit('importFinished', pageId);
    };
    importer(pageId, onFinish);
  });

}
