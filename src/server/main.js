import api from './api/index';
import config from './config';
import errorHandler from './lib/errorHandler';
import express from 'express';
import frontend from './frontend';
import session from 'express-session';
import socketio from 'socket.io';
import sockets from './sockets';
import sqlite from 'connect-sqlite3';

import {Server} from 'http';

const app = express();
const server = Server(app);
const io = socketio(server);
const SQLiteStore = sqlite(session);

app.use(session({
  resave: false,
  saveUninitialized: true,
  secret: 'kuafj82lasfk§l!1.',
  store: new SQLiteStore
}));

app.use('/api/v1', api);
app.use(frontend);

app.use(errorHandler);

io.on('connection', sockets);

const {port} = config;
server.listen(port, () => {
  console.log('Server started at port %s', config.port);
});
