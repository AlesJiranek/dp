import config from '../config';
import express from 'express';

const router = express.Router();

router.route('/login')
  .post((req, res) => {

    const {email, password} = req.body;
    const {users} = config;

    if (users.hasOwnProperty(email) && users[email] === password) {
      req.session.email = email;
      res.status(200).send({email}).end();
    }
    else
      res.status(401).end();

  });

router.route('/logout')
  .post((req, res) => {
    req.session.destroy();
    res.status(200).send({success: true}).end();
  });


export default router;
