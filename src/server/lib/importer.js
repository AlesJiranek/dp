import {exec} from 'child_process';

export default function importer(pageId = null, onFinish) {
  if (!pageId) return;

  switch (pageId) {
    case 'ivcrn': {
      exec('touch ivcrn.txt', undefined, onFinish);
    }

    case 'blokProtiIslamu': {
      exec('sleep 5', undefined, onFinish);
    }
  }

}
