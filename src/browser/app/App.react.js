import './App.scss';
import Component from 'react-pure-render/component';
import Header from './Header.react';
import Helmet from 'react-helmet';
import React, {PropTypes} from 'react';
import RouterHandler from '../../common/components/RouterHandler.react';
import mapDispatchToProps from '../../common/app/mapDispatchToProps';
import mapStateToProps from '../../common/app/mapStateToProps';
import sockets from '../sockets/decorate';
import {connect} from 'react-redux';


class App extends Component {

  static propTypes = {
    actions: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    msg: PropTypes.object.isRequired,
    search: PropTypes.object.isRequired,
    settings: PropTypes.object.isRequired
  }

  render() {
    const {actions, location: {pathname}, msg, settings: {index}, search: {list},
      history, users: {viewer}} = this.props;
    const currentQuery = list.last();

    return (
      <div data-pathname={pathname}>
        <Helmet
          meta={[{
            name: 'description',
            content: 'Application for relationships visualusations via network graph'
          }]}
          titleTemplate="%s - Social Network Visualisations"
        />
        <Header {...{msg, pathname, actions, index, currentQuery, history, viewer}} />
        <RouterHandler {...this.props} />
      </div>
    );
  }

}


App = sockets(App);
export default connect(mapStateToProps, mapDispatchToProps)(App);
