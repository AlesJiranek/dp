import AppBar from 'react-toolbox/lib/app_bar';
import {Button, IconButton} from 'react-toolbox/lib/button';
import Component from 'react-pure-render/component';
import React, {PropTypes} from 'react';
import {IndexLink, Link} from 'react-router';
import style from './Header.scss';

export default class Header extends Component {

  static propTypes = {
    actions: PropTypes.object.isRequired,
    currentQuery: PropTypes.object,
    history: PropTypes.object.isRequired,
    index: PropTypes.string,
    msg: PropTypes.object.isRequired,
    pathname: PropTypes.string.isRequired,
    viewer: PropTypes.object
  }

  toggleDrawer() {
    const {actions, history, pathname} = this.props;
    history.replaceState(null, '/');
    if (pathname === '/')
      actions.toggleDrawer();
  }

  async logout() {
    const {actions} = this.props;
    try {
      await actions.logout();
      window.location = '/';
    } catch (e) {}
  }

  render() {
    const {currentQuery, index, msg: {app: {links: msg}}, viewer} = this.props;

    return (
      <AppBar fixed flat className={style.appbar}>
        <nav className={style.navigation}>
          <ul>
            <li>
              <IconButton icon="menu" inverse onClick={() => this.toggleDrawer()} />
            </li>
            <li>
              <IndexLink activeClassName={style.active} to="/">{msg.search}</IndexLink>
            </li>
            <li>
              <Link activeClassName={style.active} to="/settings">{msg.settings}</Link>
            </li>
            <li>
              <Link activeClassName={style.active} to="/import">{msg.import}</Link>
            </li>
            {
              viewer &&
              <li>
                <Button onClick={() => this.logout()}>{msg.logout}</Button>
              </li>
            }
          </ul>
        </nav>
        <nav className={style.informations}>
          <ul>
            <li>
              <strong>Index:</strong> {index}
            </li>
            <li>
              <strong>Query:</strong> {currentQuery && currentQuery.term || 'Search query not found.'}
            </li>
          </ul>
        </nav>
      </AppBar>
    );
  }

}
