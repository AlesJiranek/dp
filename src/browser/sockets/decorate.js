import Component from 'react-pure-render/component';
import React, {PropTypes} from 'react';
import create from '../../common/sockets/create';

export default function decorate(BaseComponent) {

  return class Decorator extends Component {

    static propTypes = {
      config: PropTypes.object
    }

    componentWillMount() {
      if (!process.env.IS_BROWSER) {
        return;
      }

     // const socketsUrl = this.props.config.get('socketsUrl');
      const socketsUrl = location.origin.replace(/^http/, 'ws');
      this.sockets = create(socketsUrl);
    }

    render() {
      return <BaseComponent {...this.props} sockets={this.sockets} />;
    }

  };

}
