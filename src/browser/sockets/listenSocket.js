import Component from 'react-pure-render/component';
import React from 'react';
import * as actions from '../../common/sockets/actions';

export default function listenSocket(getArgs) {

  return BaseComponent => class ListenSocket extends Component {

    componentDidMount() {
      actions.onSocketDecoratorDidMount(this, getArgs);
    }

    componentDidUpdate() {
      actions.onSocketDecoratorDidUpdate(this, getArgs);
    }

    componentWillUnmount() {
      actions.onSocketDecoratorWillUnmount(this);
    }

    render() {
      return <BaseComponent {...this.props} />;
    }

  };

}
