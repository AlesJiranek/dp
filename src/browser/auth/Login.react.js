import './Login.styl';
import Component from 'react-pure-render/component';
import Helmet from 'react-helmet';
import Input from 'react-toolbox/lib/input';
import React, {PropTypes} from 'react';
import focusInvalidField from '../lib/focusInvalidField';

export default class Login extends Component {

  static propTypes = {
    actions: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    msg: PropTypes.object.isRequired
  }

  async onFormSubmit(e) {
    e.preventDefault();
    const {actions, auth} = this.props;
    try {
      await actions.login(auth.form.fields);
      this.redirectAfterLogin();
    } catch (e) {
      focusInvalidField(this, e);
    }
  }


  redirectAfterLogin() {
    const {history, location} = this.props;
    if (location.state && location.state.nextPathname)
      history.replaceState(null, location.state.nextPathname);
    else
      history.replaceState(null, '/');
  }

  render() {
    const {actions, auth: {form}, msg: {auth: {form: msg}}} = this.props;

    return (
      <div className="login">
        <Helmet title="Login" />
        <form onSubmit={e => this.onFormSubmit(e)}>
          <fieldset disabled={form.disabled}>
            <legend>{msg.legend}</legend>
            <Input
              label={msg.label.email}
              name="email"
              onChange={v => actions.onAuthFormFieldChange('email', v)}
              value={form.fields.email}
            />
            <br />
            <Input
              label={msg.label.password}
              name="password"
              onChange={v => actions.onAuthFormFieldChange('password', v)}
              type="password"
              value={form.fields.password}
            />
            <br />
            <button
              children={msg.button.login}
              type="submit"
            />
            {form.error &&
              <p className="error-message">{form.error.message}</p>
            }
          </fieldset>
        </form>
      </div>
    );
  }

}
