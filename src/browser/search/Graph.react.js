import Component from 'react-pure-render/component';
import React, {PropTypes} from 'react';
import d3 from 'd3';


export default class Graph extends Component {

  static propTypes = {
    height: PropTypes.number,
    links: PropTypes.object,
    nodes: PropTypes.object,
    width: PropTypes.number
  }


  componentWillUnmount() {
    d3.select('#graph > svg').remove();
  }


  render() {
    let {links, nodes} = this.props;
    const {width, height} = this.props;

    nodes = nodes.toJS();
    links = links.toJS();

    const force = d3.layout.force()
      .charge(-220)
      .linkDistance(30)
      .size([width, height])
      .nodes(nodes)
      .links(links);

    d3.select('#graph > svg').remove();

    const svg = d3.select('#graph').append('svg')
      .style('width', '100%')
      .style('height', '100%')
      .attr('width', width)
      .attr('height', height);

    const link = svg.selectAll('.link')
      .data(links)
      .enter()
      .append('path')
      .attr('class', 'link')
      .style('stroke-width', d => Math.sqrt(d.value))
      .style('stroke', d => {
        switch (d.level) {
          case -1: return '#555';
          case 0: return '#999';
          case 1: return '#ccc';
        }
      })
      .style('fill', 'none');

    const node = svg.selectAll('.node')
      .data(nodes)
      .enter()
      .append('circle')
      .attr('class', 'node')
      .attr('r', d => d.type === 'page' ? 10 : 5)
      .on('mouseover', (e) => {
        if (e.name === 'Unknown') return;

        d3.select('#graph')
          .append('div')
          .attr('class', 'node-tooltip')
          .style('left', `${e.x + 10}px`)
          .style('top', `${e.y + 64}px`);

        d3.select(`#graph > .node-tooltip`)
          .append('h6')
          .text(e.name);

        d3.select(`#graph > .node-tooltip`)
          .append('img')
          .attr('src', `http://graph.facebook.com/${e.id}/picture`);

      })
      .on('mouseout', () => d3.selectAll(`#graph > .node-tooltip`).remove())
      .style('fill', ({type, name}) => {
        let color = '#3333ff';

        if (type === 'page')
          color = '#cc0000';
        else if (name === 'Unknown')
          color = '#99ff00';

        return color;
      })
      .call(force.drag);

    node.append('title')
      .text(d => d.name);

    force.on('tick', () => {

      link.attr('d', d => {
        const x1 = d.source.x;
        const y1 = d.source.y;
        let x2 = d.target.x;
        let y2 = d.target.y;
        const dx = x2 - x1;
        const dy = y2 - y1;
        const dr = Math.sqrt(dx * dx + dy * dy);

          // Defaults for normal edge.
        let drx = dr;
        let dry = dr;
        let xRotation = 0; // degrees
        let largeArc = 0; // 1 or 0
        const sweep = 1; // 1 or 0

          // Self edge.
        if (x1 === x2 && y1 === y2) {
          // Fiddle with this angle to get loop oriented.
          xRotation = -45;

          // Needs to be 1.
          largeArc = 1;

          // Make drx and dry different to get an ellipse
          // instead of a circle.
          drx = 30;
          dry = 20;

          // For whatever reason the arc collapses to a point if the beginning
          // and ending points of the arc are the same, so kludge it.
          x2 = x2 + 1;
          y2 = y2 + 1;
        }

        return `M${x1},${y1}A${drx},${dry} ${xRotation},${largeArc},${sweep} ${x2},${y2}`;
      });

      // node.attr('cx', (d) => d.x)
        // .attr('cy', (d) => d.y);
      node.attr('transform', d => `translate(${d.x}, ${d.y})`);
    });

    force.start();

    return (
      <span />
    );
  }

}
