import Component from 'react-pure-render/component';
import ClassNames from 'classnames';
import React, {PropTypes} from 'react';
import style from 'react-toolbox/lib/list/style.scss';
import FontIcon from 'react-toolbox/lib/font_icon';


export default class Query extends Component {

  static propTypes = {
    actions: PropTypes.object,
    query: PropTypes.object
  }


  editQuery() {
    const {actions, query: {id}} = this.props;
    actions.editQuery(id);
  }


  render() {
    const {actions, query} = this.props;
    const times = [];
    query.timeFrom ? times.push(query.timeFrom) : null;
    query.timeTo ? times.push(query.timeTo) : null;


    return (
      <li>
        <span className={style.item}>
          <span className={style.itemContentRoot} onClick={() => this.editQuery()}>
            <span className={ClassNames(style.itemText, style.primary)}>{query.term}</span>
            <span className={style.itemText}>{times.join(' - ')}</span>
          </span>
          <span style={{cursor: 'pointer'}}>
            <FontIcon
              onClick={() => actions.deleteQuery(query.id)}
              value="delete"
            />
            </span>
        </span>
      </li>
    );

  }

}
