import Component from 'react-pure-render/component';
import Drawer from 'react-toolbox/lib/drawer';
import Graph from './Graph.react';
import Helmet from 'react-helmet';
import List from './List.react';
import NewQuery from './NewQuery.react';
import React, {PropTypes} from 'react';
import listenSocket from '../sockets/listenSocket';
import style from './Page.scss';


class Page extends Component {

  static propTypes = {
    actions: PropTypes.object.isRequired,
    msg: PropTypes.object,
    search: PropTypes.object.isRequired,
    settings: PropTypes.object.isRequired,
    sockets: PropTypes.object.isRequired
  }


  componentDidMount() {
    const {actions, settings: {index}, sockets, search: {list}} = this.props;
    this.setWindowSize();

    if (!index) sockets.getIndexes();

    if (list.size === 0)
      actions.toggleDrawer();
  }


  setWindowSize() {
    const {actions} = this.props;
    const {graph} = this.refs;

    const width = graph.clientWidth;
    const height = window.innerHeight - 64; // navbar height
    actions.setWindowSize(width, height);
  }


  render() {
    const {actions, msg: {search: msg}, search: {newQuery, list, nodes, links, drawer, windowWidth, windowHeight}, sockets, settings} = this.props;
    return (
      <div className="search-page">
        <Helmet title={msg.title} />
        <div ref="graph">
          {
            windowWidth && windowHeight &&
            <Graph {...{nodes, links}} height={windowHeight} width={windowWidth} />
          }
        </div>
        <Drawer active={drawer} className={style.drawer} onOverlayClick={actions.toggleDrawer}>
          <NewQuery {...{actions, msg, newQuery, sockets, settings}}/>
          <List {...{actions, list, msg}} />
        </Drawer>
      </div>
    );

  }

}

Page = listenSocket((props, sockets) => ({
  action: props.actions.onSearchResult,
  eventType: sockets.eventTypes.SEARCH_RESULTS
}))(Page);

Page = listenSocket((props, sockets) => ({
  action: props.actions.setAvailableIndexes,
  eventType: sockets.eventTypes.AVAILABLE_INDEXES
}))(Page);

export default Page;
