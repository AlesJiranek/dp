import Component from 'react-pure-render/component';
import React, {PropTypes} from 'react';
import Input from 'react-toolbox/lib/input';

export default class Query extends Component {

  static propTypes = {
    actions: PropTypes.object,
    msg: PropTypes.object,
    newQuery: PropTypes.object,
    settings: PropTypes.object,
    sockets: PropTypes.object
  };


  onChange({target: {name, value}}) {
    const {actions} = this.props;
    actions.onNewQueryChange(name, value);
  }


  onKeyUp(e) {
    const {actions, newQuery, sockets, settings: {index, sizeLimit}} = this.props;

    if (e.key === 'Enter' && newQuery.term.trim()) {
      actions.addQuery(newQuery, sockets, index, sizeLimit);
    }
  }


  render() {
    const {msg, newQuery} = this.props;

    return (
      <div className="search-query">

        <Input
          id="term"
          label={msg.term}
          name="term"
          onChange={(n, e) => this.onChange(e)}
          onKeyUp={(e) => this.onKeyUp(e)}
          type="text"
          value={newQuery.term}
        />

        <Input
          id="timeFrom"
          label={msg.timeFrom}
          name="timeFrom"
          onChange={(n, e) => this.onChange(e)}
          onKeyUp={(e) => this.onKeyUp(e)}
          type="text"
          value={newQuery.timeFrom}
        />

        <Input
          id="timeTo"
          label={msg.timeTo}
          name="timeTo"
          onChange={(n, e) => this.onChange(e)}
          onKeyUp={(e) => this.onKeyUp(e)}
          type="text"
          value={newQuery.timeTo}
        />

        <Input
          id="size"
          label={msg.sizeLimit}
          name="sizeLimit"
          onChange={(n, e) => this.onChange(e)}
          onKeyUp={(e) => this.onKeyUp(e)}
          type="text"
          value={newQuery.sizeLimit}
        />

      </div>
    );
  }

}
