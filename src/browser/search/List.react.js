import Component from 'react-pure-render/component';
import TList from 'react-toolbox/lib/list/List';
import React, {PropTypes} from 'react';
import Query from './Query.react';

export default class List extends Component {

  static propTypes = {
    actions: PropTypes.object.isRequired,
    list: PropTypes.object.isRequired,
    msg: PropTypes.object.isRequired
  }


  render() {
    const {actions, list, msg} = this.props;


    if (!list.size)
      return <p>{msg.empty}</p>;

    return (
      <TList>
        {list
          .reverse()
          .map(query =>
            <Query {...{actions, query, msg}} key={query.id} />
        )}
      </TList>
    );

  }

}
