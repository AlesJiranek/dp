import Component from 'react-pure-render/component';
import Button from 'react-toolbox/lib/button';
import Dropdown from 'react-toolbox/lib/dropdown';
import React, {PropTypes} from 'react';
import listenSocket from '../sockets/listenSocket';
import style from './Page.scss';


class Page extends Component {

  static propTypes = {
    actions: PropTypes.object.isRequired,
    msg: PropTypes.object,
    importPages: PropTypes.object.isRequired,
    sockets: PropTypes.object.isRequired
  }

  onChange(pageId) {
    const {actions} = this.props;
    actions.setPageToImport(pageId);
  }

  getSource(pages, inProgress) {
    return pages.filter(p => !inProgress.includes(p.id))
      .map(({id, name}) => ({label: name, value: id}))
      .toList()
      .toArray();
  }


  startImport() {
    const {actions, importPages: {selectedPage}, sockets} = this.props;
    if (!selectedPage) return;
    actions.startImport(sockets, selectedPage);
  }


  render() {
    const {msg: {import: msg}, importPages: {pages, selectedPage, inProgress}} = this.props;

    return (
      <div className="import-page page">
        <h1>{msg.title}</h1>
        <Dropdown
          auto
          label={msg.pages}
          onChange={e => this.onChange(e)}
          source={this.getSource(pages, inProgress)}
          value={selectedPage}
        />
        <Button
          label={msg.start}
          raised
          onClick={() => this.startImport()}
          primary
        />

        <h2 className={style.inProgress}>{msg.inProgress.title}</h2>
        {
          inProgress.size
          ? <ul className={style.inProgressList}>
              {
                inProgress.map(pageId =>
                  <li key={pageId}>
                    {pages.get(pageId).name}
                    <span className={style.spinner}/>
                  </li>
                )
              }
            </ul>
          : <div>{msg.inProgress.noPages}</div>
        }
      </div>
    );

  }

}


Page = listenSocket((props, sockets) => ({
  action: props.actions.importFinished,
  eventType: sockets.eventTypes.IMPORT_FINISHED
}))(Page);

export default Page;
