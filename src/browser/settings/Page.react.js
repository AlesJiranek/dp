import Component from 'react-pure-render/component';
import Dropdown from 'react-toolbox/lib/dropdown';
import Input from 'react-toolbox/lib/input';
import React, {PropTypes} from 'react';
import listenSocket from '../sockets/listenSocket';


class Page extends Component {

  static propTypes = {
    actions: PropTypes.object.isRequired,
    msg: PropTypes.object,
    settings: PropTypes.object.isRequired,
    sockets: PropTypes.object.isRequired
  }


  componentDidMount() {
    const {sockets} = this.props;
    sockets.getIndexes();
  }


  getSource(availableIndexes) {
    const source = [];
    source.push({label: 'All', value: '_all'});
    availableIndexes.map(idx => source.push({label: idx, value: idx}));
    return source;
  }


  onChange(index) {
    const {actions} = this.props;
    actions.setIndex(index);
  }

  onChangeMapping({target: {name, value}}) {
    const {actions} = this.props;
    actions.setMappingField(name, value);
  }


  onChangeLimit({target: {value}}) {
    const {actions} = this.props;
    actions.setSizeLimit(value);
  }


  render() {
    const {msg: {settings: msg}, settings: {index, availableIndexes, sizeLimit, mapping}} = this.props;

    return (
      <div className="settings-page page">
        <h1>{msg.title}</h1>
        {
          availableIndexes.size !== 0 &&
          <Dropdown
            auto
            label={msg.index}
            onChange={e => this.onChange(e)}
            source={this.getSource(availableIndexes)}
            value={index}
          />
        }

        <Input
          id="sizeLimit"
          label={msg.sizeLimit}
          name="term"
          onChange={(n, e) => this.onChangeLimit(e)}
          type="text"
          value={sizeLimit}
        />

        <h2>{msg.mappingTitle}</h2>

        <Input
          id="page"
          label={msg.mapping.page}
          name="page"
          onChange={(n, e) => this.onChangeMapping(e)}
          type="text"
          value={mapping.page}
        />

        <Input
          id="userId"
          label={msg.mapping.userId}
          name="userId"
          onChange={(n, e) => this.onChangeMapping(e)}
          type="text"
          value={mapping.userId}
        />

        <Input
          id="userName"
          label={msg.mapping.userName}
          name="userName"
          onChange={(n, e) => this.onChangeMapping(e)}
          type="text"
          value={mapping.userName}
        />

        <Input
          id="postId"
          label={msg.mapping.postId}
          name="postId"
          onChange={(n, e) => this.onChangeMapping(e)}
          type="text"
          value={mapping.postId}
        />

        <Input
          id="parentPostId"
          label={msg.mapping.parentPostId}
          name="parentPostId"
          onChange={(n, e) => this.onChangeMapping(e)}
          type="text"
          value={mapping.parentPostId}
        />

        <Input
          id="level"
          label={msg.mapping.level}
          name="level"
          onChange={(n, e) => this.onChangeMapping(e)}
          type="text"
          value={mapping.level}
        />
      </div>
    );

  }

}

Page = listenSocket((props, sockets) => ({
  action: props.actions.setAvailableIndexes,
  eventType: sockets.eventTypes.AVAILABLE_INDEXES
}))(Page);

export default Page;
