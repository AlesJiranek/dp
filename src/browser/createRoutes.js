import App from './app/App.react';
import Auth from './auth/Page.react';
import ImportPages from './import/Page.react';
import NotFound from './notfound/Page.react';
import React from 'react';
import Search from './search/Page.react';
import Settings from './settings/Page.react';
import {IndexRoute, Route} from 'react-router';

export default function createRoutes(getState) {

  function requireAuth(nextState, replaceState) {
    const loggedInUser = getState().users.viewer;
    if (!loggedInUser) {
      replaceState({nextPathname: nextState.location.pathname}, '/login');
    }
  }

  return (
    <Route component={App} path="/">
      <IndexRoute component={Search} />
      <Route component={Auth} path="login" />
      <Route component={Settings} onEnter={requireAuth} path="settings" />
      <Route component={ImportPages} onEnter={requireAuth} path="import" />
      <Route component={NotFound} path="*" />
    </Route>
  );

}
