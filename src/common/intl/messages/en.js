export default {
  app: {
    links: {
      search: 'Search',
      settings: 'Settings',
      import: 'Import',
      logout: 'Log out'
    }
  },
  auth: {
    form: {
      button: {
        login: 'Login',
        signup: 'Sign up'
      },
      legend: 'Login',
      label: {
        email: 'E-mail',
        password: 'Password'
      },
      wrongPassword: 'Wrong password.'
    },
    logout: {
      button: 'Logout'
    },
    login: {
      title: 'Login'
    },
    validation: {
      email: 'Email address is not valid.',
      password: 'Password must contain at least {minLength} characters.',
      required: `Please fill out {prop, select,
        email {email}
        password {password}
        other {'{prop}'}
      }.`
    }
  },
  import: {
    title: 'Import page',
    pages: 'Select page to import',
    start: 'Start',
    inProgress: {
      title: 'Currently imported pages',
      noPages: 'Currently there is no ongoing import.'
    }
  },
  notFound: {
    continueMessage: 'Continue here please.',
    header: 'This page isn\'t available',
    message: 'The link may be broken, or the page may have been removed.',
    title: 'Page Not Found'
  },
  search: {
    title: 'Search...',
    empty: `It's rather empty here...`,
    term: 'Term',
    timeFrom: 'From',
    timeTo: 'To',
    sizeLimit: 'Result size limit'
  },
  settings: {
    title: 'Settings',
    index: 'Index',
    sizeLimit: 'Maximum response size',
    mappingTitle: 'Fields Mapping',
    mapping: {
      page: 'Page',
      userId: 'User Id',
      userName: 'User Name',
      postId: 'Post Id',
      parentPostId: 'Parent Post Id',
      level: 'Level'
    }
  }
};
