import io from 'socket.io-client';


export default function create(socketsUrl) {
  const sockets = io.connect(socketsUrl);


  return {
    root: sockets,


    eventTypes: {
      SEARCH_RESULTS: 'searchResults',
      AVAILABLE_INDEXES: 'availableIndexes',
      IMPORT_FINISHED: 'importFinished'
    },


    search(index, query) {
      sockets.emit('search', index, query);
    },


    getIndexes() {
      sockets.emit('getIndexes');
    },


    startImport(pageId) {
      sockets.emit('startImport', pageId);
    }
  };

}
