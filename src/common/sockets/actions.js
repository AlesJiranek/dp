
function listenSocketEvent(decorator, getArgs) {
  const {props, props: {sockets}} = decorator;
  const {action, eventType} = getArgs(props, sockets);

  if (decorator.eventType) {
    const refChanged = decorator.eventType !== eventType;
    if (!refChanged) return;

    sockets.root.removeListener(decorator.eventType, decorator.listener);
  }

  decorator.eventType = eventType;

  const listener = (...args) => {
    action(...[...args]);
  };

  decorator.listener = listener;

  sockets.root.on(eventType, listener);
}


export function onSocketDecoratorDidMount(decorator, getArgs) {
  listenSocketEvent(decorator, getArgs);
}


export function onSocketDecoratorDidUpdate(decorator, getArgs) {
  listenSocketEvent(decorator, getArgs);
}


export function onSocketDecoratorWillUnmount(decorator) {
  if (decorator.eventType) {
    const {props: {sockets}} = decorator;
    sockets.root.removeListener(decorator.eventType, decorator.listener);
  }
}
