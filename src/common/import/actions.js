export const SET_PAGE_TO_IMPORT = 'SET_PAGE_TO_IMPORT';
export const START_IMPORT = 'START_IMPORT';
export const IMPORT_FINISHED = 'IMPORT_FINISHED';

export function setPageToImport(pageId) {
  return ({
    type: SET_PAGE_TO_IMPORT,
    payload: {
      pageId
    }
  });
}


export function startImport(sockets, pageId) {
  sockets.startImport(pageId);
  return ({
    type: START_IMPORT,
    payload: {
      pageId
    }
  });
}


export function importFinished(pageId) {
  return ({
    type: IMPORT_FINISHED,
    payload: {
      pageId
    }
  });
}
