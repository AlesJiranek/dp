import * as actions from './actions';
import Page from './page';
import {Map, List, Record} from 'immutable';

const InitialState = Record({
  pages: Map(),
  selectedPage: null,
  inProgress: List()
});
const initialState = new InitialState;

function revive({pages}) {
  return initialState.merge({
    pages: Map(pages).map(p => new Page(p))
  });
}

export default function importReducer(state = initialState, action) {
  if (!(state instanceof InitialState)) return revive(state);

  switch (action.type) {

    case actions.SET_PAGE_TO_IMPORT: {
      const {pageId} = action.payload;
      return state.set('selectedPage', pageId);
    }


    case actions.START_IMPORT: {
      const {pageId} = action.payload;
      return state.update('inProgress', inProgress => inProgress.push(pageId))
        .set('selectedPage', null);
    }


    case actions.IMPORT_FINISHED: {
      const {pageId} = action.payload;
      return state.update('inProgress', inProgress =>
        inProgress.delete(inProgress.indexOf(pageId))
      );
    }

  }

  return state;
}
