export const SET_AVAILABLE_INDEXES = 'SET_AVAILABLE_INDEXES';
export const SET_INDEX = 'SET_INDEX';
export const SET_MAPPING_FIELD = 'SET_MAPPING_FIELD';
export const SET_SIZE_LIMIT = 'SET_SIZE_LIMIT';

export function setAvailableIndexes(indexes) {
  const availableIndexes = [];
  for (const index in indexes) {
    if (indexes.hasOwnProperty(index)) {
      availableIndexes.push(index);
    }
  }

  return ({
    type: SET_AVAILABLE_INDEXES,
    payload: {availableIndexes}
  });
}


export function setIndex(index) {
  return ({
    type: SET_INDEX,
    payload: {index}
  });
}


export function setSizeLimit(limit) {
  return ({
    type: SET_SIZE_LIMIT,
    payload: {limit}
  });
}


export function setMappingField(name, value) {
  return ({
    type: SET_MAPPING_FIELD,
    payload: {
      name,
      value
    }
  });
}
