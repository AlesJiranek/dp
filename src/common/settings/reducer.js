import * as actions from './actions';
import {List, Record} from 'immutable';

const InitialState = Record({
  index: 'extrem_fb', // '_all',
  availableIndexes: List(),
  sizeLimit: 6000,
  mapping: new (Record({
    page: 'page',
    userId: 'userId',
    userName: 'userName',
    postId: 'id',
    parentPostId: 'parentId',
    level: 'level'
  }))
});
const initialState = new InitialState;

const revive = ({index, availableIndexes, sizeLimit}) => initialState.merge({
  index,
  availableIndexes: availableIndexes.map(idx => idx),
  sizeLimit
});

export default function usersReducer(state = initialState, action) {
  if (!(state instanceof InitialState)) return revive(state);

  switch (action.type) {

    case actions.SET_AVAILABLE_INDEXES: {
      const {availableIndexes} = action.payload;
      return state.set('availableIndexes', List(availableIndexes));
    }


    case actions.SET_INDEX: {
      const {index} = action.payload;
      return state.set('index', index);
    }


    case actions.SET_MAPPING_FIELD: {
      const {name, value} = action.payload;
      return state.setIn(['mapping', name], value);
    }


    case actions.SET_SIZE_LIMIT: {
      const {limit} = action.payload;
      return state.set('sizeLimit', limit);
    }

  }

  return state;
}
