import parseResults from './parseResults';

export const ON_NEW_QUERY_CHANGE = 'ON_NEW_QUERY_CHANGE';
export const ADD_QUERY = 'ADD_QUERY';
export const EDIT_QUERY = 'EDIT_QUERY';
export const DELETE_QUERY = 'DELETE_QUERY';
export const ON_SEARCH_RESULTS = 'ON_SEARCH_RESULTS';
export const TOGGLE_DRAWER = 'TOGGLE_DRAWER';
export const SET_WINDOW_SIZE = 'SET_WINDOW_SIZE';


export function onNewQueryChange(name, value) {
  return ({
    type: ON_NEW_QUERY_CHANGE,
    payload: {name, value}
  });
}


export function addQuery(query, sockets, index) {

  sockets.search(index, query);

  return ({
    type: ADD_QUERY,
    payload: {query}
  });
}


export function deleteQuery(id) {
  return ({
    type: DELETE_QUERY,
    payload: {id}
  });
}


export function editQuery(id) {
  return ({
    type: EDIT_QUERY,
    payload: {id}
  });
}


export function onSearchResult(result) {

  const parse = ({store: {getState}}) => {
    const {settings: {mapping}} = getState();
    return parseResults(result, mapping);
  };

  return (deps) => {
    return ({
      type: ON_SEARCH_RESULTS,
      payload: parse(deps)
    });
  };
}


export function toggleDrawer() {
  return ({
    type: TOGGLE_DRAWER
  });
}


export function setWindowSize(width, height) {
  return ({
    type: SET_WINDOW_SIZE,
    payload: {
      width,
      height
    }
  });
}
