import {Map} from 'immutable';


/**
 * Maps fields
 */
const mapFields = (_source, fieldsMapping) => {
  const page = _source[fieldsMapping.page];
  const userId = _source[fieldsMapping.userId];
  const userName = _source[fieldsMapping.userName];
  const id = _source[fieldsMapping.postId];
  const parentId = _source[fieldsMapping.parentPostId];
  const level = _source[fieldsMapping.level];

  return {page, userId, userName, id, parentId, level};
};


/**
 * Parses elastic search result based on given fields mapping
 */
export default function parseResults(result, fieldsMapping) {
  const {hits: {hits}} = result;
  let users = Map();
  let pages = Map();
  let posts = Map();
  let links = Map();

  hits.map(({_source}) => {
    const {page, userId, userName, id, parentId, level} = mapFields(_source, fieldsMapping);

    pages = pages.set(page, page);
    users = users.set(userId, {id: userId, name: userName});
    posts = posts.set(id, {parentId, author: userId, id, page, level});

    if (level === -1) {
      if (links.has([page, userId])) {
        links = links.update([page, userId], link => link.value++);
      } else {
        links = links.set([page, userId], {parent: page, child: userId, value: 1, level});
      }
    }
  });

  posts
  .forEach(post => {
    const {parentId, author, level, page} = post;

    if (level === -1) return;

    let parentPost = posts.get(parentId);

    if (!parentPost) {
      const unknownUserId = ['user', page, level - 1].join('_');
      users = users.set(unknownUserId, {id: unknownUserId, name: 'Unknown'});
      parentPost = {id: parentId, author: unknownUserId};

      if (level === 0) {
        links = links.set([page, unknownUserId], {parent: page, child: unknownUserId, value: 1, level});
      }

      if (level === 1) {
        const unknownUserIdLower = ['user', page, -1].join('_');
        users = users.set(unknownUserIdLower, {id: unknownUserIdLower, name: 'Unknown'});

        const parentParentPostId = ['page', page, -1].join('_');
        const parentParentPost = {id: parentParentPostId, author: unknownUserIdLower, page, level: -1};
        posts = posts.set(parentParentPostId, parentParentPost);

        parentPost[parentId] = parentParentPostId;

        links = links.set([page, unknownUserIdLower], {parent: page, child: unknownUserIdLower, value: 1, level: -1});
        links = links.set([unknownUserIdLower, unknownUserId], {parent: unknownUserIdLower, child: unknownUserId, value: 1, level: 0});
      }

      posts = posts.set(parentId, parentPost);
    }

    const {author: parentAuthor} = parentPost;

    if (parentAuthor) {
      links = links.set([parentAuthor, author], {parent: parentAuthor, child: author, value: 1, level});
    }
  });

  return {pages, users, links};
}
