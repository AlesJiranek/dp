import * as actions from './actions';
import Query from './query';
import getRandomString from '../lib/getRandomString';
import {List, Map, Record} from 'immutable';


const InitialState = Record({
  list: List(),
  newQuery: new Query,
  nodes: List(),
  links: List(),
  drawer: false,
  windowWidth: null,
  windowHeight: null
});
const initialState = new InitialState;


const revive = ({list, newQuery}) => initialState.merge({
  list: list.map(query => new Query(query)),
  newQuery: new Query(newQuery)
});


export default function searchReducer(state = initialState, action) {
  if (!(state instanceof InitialState)) return revive(state);


  switch (action.type) {

    case actions.ON_NEW_QUERY_CHANGE: {
      const {name, value} = action.payload;
      return state.setIn(['newQuery', name], value);
    }


    case actions.ADD_QUERY: {
      const {query} = action.payload;
      const newQuery = query.merge({
        id: getRandomString(),
        term: query.term.trim()
      });

      return state.update('list', list => list.push(newQuery));
    }


    case actions.DELETE_QUERY: {
      const {id} = action.payload;
      return state.update('list', list =>
        list.delete(list.findIndex(query => query.id === id))
      );
    }


    case actions.EDIT_QUERY: {
      const {id} = action.payload;
      const index = state.list.findIndex(q => q.id === id);
      const query = state.list.get(index);

      return state.set('newQuery', query);
    }


    case actions.ON_SEARCH_RESULTS: {
      const {payload: {pages, users, links}} = action;
      let mapper = Map();
      return state.withMutations(state => {
        state.set('nodes', pages
          .toList()
          .map((page, idx) => {
            mapper = mapper.set(page, users.size + idx);
            return new (Record({name: page, id: page, type: 'page'}));
          })
        );

        state.update('nodes', nodes =>
          users
            .toList()
            .map((user, idx) => {
              mapper = mapper.set(user.id, idx);
              return new (Record({name: user.name, id: user.id, type: 'user'}));
            })
            .concat(nodes)
        );

        let finalLinks = Map();

        links.forEach(link => {
          const parentIndex = mapper.get(link.parent);
          const childIndex = mapper.get(link.child);

          const existingLinkIndex = finalLinks.has(`${parentIndex}_${childIndex}`);

          if (existingLinkIndex) {
            finalLinks = finalLinks.updateIn([`${parentIndex}_${childIndex}`, 'value'], value => value + 1);
          } else {
            finalLinks = finalLinks.set(`${parentIndex}_${childIndex}`, new (Record({source: parentIndex, target: childIndex, value: 1, level: link.level})));
          }

          state.set('links', finalLinks.toList());
        });
      });
    }


    case actions.TOGGLE_DRAWER : {
      return state.update('drawer', drawer => !drawer);
    }


    case actions.SET_WINDOW_SIZE: {
      const {width, height} = action.payload;

      return state
        .set('windowWidth', width)
        .set('windowHeight', height);
    }

  }


  return state;
}
