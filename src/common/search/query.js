import {Record} from 'immutable';

const query = Record({
  id: '',
  term: '',
  timeFrom: '',
  timeTo: '',
  sizeLimit: null
});

export default query;
