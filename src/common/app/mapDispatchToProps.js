import * as authActions from '../auth/actions';
import * as importActions from '../import/actions';
import * as searchActions from '../search/actions';
import * as settingsActions from '../settings/actions';
import * as uiActions from '../ui/actions';
import {Map} from 'immutable';
import {bindActionCreators} from 'redux';

const actions = [
  authActions,
  importActions,
  searchActions,
  settingsActions,
  uiActions
];

export default function mapDispatchToProps(dispatch) {
  const creators = Map()
    .merge(...actions)
    .filter(value => typeof value === 'function')
    .toObject();

  return {
    actions: bindActionCreators(creators, dispatch),
    dispatch
  };
}
