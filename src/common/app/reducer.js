import {combineReducers} from 'redux';

// Note we are composing all reducers. Web, native, whatever. Of course we can
// pass platform specific reducers in configureStore, but there is no reason to
// do that, until app is really large.
import auth from '../auth/reducer';
import device from '../device/reducer';
import importPages from '../import/reducer';
import intl from '../intl/reducer';
import search from '../search/reducer';
import settings from '../settings/reducer';
import ui from '../ui/reducer';
import users from '../users/reducer';

const appReducer = combineReducers({
  auth,
  device,
  importPages,
  intl,
  search,
  settings,
  ui,
  users
});

export default appReducer;
