# Network Graph Analyzer

## Prerequisites

- [node.js](http://nodejs.org) (v5 is recommended).
- [gulp](http://gulpjs.com/) (`npm install -g gulp`)

## Installing

```
npm install
```

## Start Development

- run `gulp`
- point your browser to [localhost:8000](http://localhost:8000)

## Dev Tasks

- `gulp` run web app in development mode
- `gulp -p` run web app in production mode
- `gulp test` test app
